# This script was updated to run using julia 1.8.1.
# this tutorial was modified from the linear regression tutorial:
# https://turing.ml/dev/tutorials/05-linear-regression/
# James M. Gorman
# 2022-09-27

using Base

# Import Turing, Distributions, and Optim (for calculating MLE/MAP)
using Turing, Distributions, Optim, StatsBase

# Import MCMCChains, Plots, and StatPlots for visualizations and diagnostics.
using MCMCChains, Plots, StatsPlots

# Functionality for splitting and normalizing the data.
using MLDataUtils: shuffleobs, splitobs, rescale!, stratifiedobs

using SpecialFunctions, CSV, Tables, DataFrames

# Set a seed for reproducibility.
using Random
Random.seed!(0)

# Hide the progress prompt while sampling.
Turing.setprogress!(false);


#cdf of the normal distribution
#using multiple options in case a single value or a vector is input...
function normal_cdf(x,mu,sigma)
	if length(x)==1
		return 0.5*(1 + erf((x - mu)/(sigma*sqrt(2))))
	elseif length(x)==0
		return 0
	else
		temp = ones(Float64,length(x))
		for a in 1:length(x)
			temp[a] = 0.5*(1 + erf((x[a] - mu)/(sigma*sqrt(2))))
		end
		return temp
	end
end

#determine whether or not the result is a 0 or 1 given v and mu
function result(input_v, input_cdf, mu)
	v=input_v
	x=input_cdf
	if length(v)!=length(x)
		return Missing
	else
		if length(x)==1
			trial = rand()
			if trial>x && v>mu
				return 0.
			elseif trial>x && v<mu
				return 0.
			else
				return 1
			end
		elseif length(x)==0
			return 0
		else
			trial = rand(length(x))
			result = ones(length(x))
			for a in 1:length(result)
				if trial[a]>x[a] && v[a]>mu
					result[a] = 0.
				elseif trial[a]>x[a] && v[a]<mu
					result[a] = 0.
				end
			end
			return result
		end
	end
end

equally_spaced_file = "equally-spaced_regression_data.csv"

mean_n = 1000.
stdev_n = 100.

static_input = (mean_n-3*stdev_n):(6*stdev_n/20):(mean_n+3*stdev_n)
static_cdf = normal_cdf(static_input, mean_n, stdev_n)
static_result = result(static_input, static_cdf, mean_n)
equal_spaced_data = hcat(static_input, static_result)

interp_pts = 22

#import fake data...
static_data = CSV.File(equally_spaced_file, header=[:x, :y]) |> DataFrame

# Split our dataset 50%/50% into training/test sets.
train_data1, test_data1 = splitobs(shuffleobs(static_data), 0.5)

data = vcat(train_data1,test_data1)

#print size of the data (for verification)
size(train_data1)
size(test_data1)

l_train, w_train = size(train_data1)
l_test, w_test = size(test_data1)

l_tot = l_train + l_test
w_tot = w_train + w_test

# Split our dataset into the training/test sets.
train_data, test_data = splitobs(data, l_train/l_tot)

# Turing requires data in matrix form.
target = :y
train = Matrix(select(train_data, Not(target)))
test = Matrix(select(test_data, Not(target)))
train_target = train_data[:, target]
test_target = test_data[:, target]

# Standardize the features.
mu, sigma = rescale!(train; obsdim = 1)
rescale!(test, mu, sigma; obsdim = 1)

# Standardize the targets.
mu_target, sigma_target = rescale!(train_target; obsdim = 1)
rescale!(test_target, mu_target, sigma_target; obsdim = 1);

percent_offset = 0.

# Bayesian gaussian CDF regression
@model function regression(x, y)

	#concatenate data
	temp_dat = DataFrame(c1=x, c2=y)
	#separate it out
	dat0 = filter(row -> row.c2 in 0, temp_dat)
	dat1 = filter(row -> row.c2 in 1, temp_dat)
	#max 0 and min 1
	max0 = maximum(dat0[:,1])
	min1 = minimum(dat1[:,1])

	#TriangularDist(a, b, c)     # Triangular distribution with lower limit a, upper limit b, and mode c
	#mu mode will be halfway b/t the highest 0 and lowest 1
	mumu ~ TriangularDist(minimum(x)*1, maximum(x)*1, 0.5*(max0 + min1))

	#sigma mode will be half of the distance b/t the highest 0 and lowest 1
	sigsig ~ TriangularDist(0, 0.5*(maximum(x) - minimum(x)), abs(0.5*(max0 - min1)))
	
	n = length(x)

    for i = 1:n
        v = 0.5*(1 + erf((x[i] - mumu)/(sigsig*sqrt(2))))
        y[i] ~ Bernoulli(v)
    end
end

chain_header = Tables.table(["mu" "sigma"])
io_raw_chain = open("chain_data_output.csv","a")
CSV.write(io_raw_chain, chain_header; delim = ',', header = false)


solver_method = "HMC"
#solver_method = "NUTS"
#solver_method = "NUTSMCMC"

iterations = 10000
eps_val = 0.05
tau_val = 10

model = regression(data.x, data.y)
if solver_method=="HMC"
	chain = sample(model, HMC(eps_val, tau_val), iterations, progress=false)
	plot_out = "chain_uniform--hmc--4.png"
	file_out = "output--HMC.csv"
	hist_out_mu = "histogram--chain_uniform--hmc--mu.png"
	hist_out_sigma = "histogram--chain_uniform--hmc--sigma.png"
elseif solver_method=="NUTS"
	chain = sample(model, NUTS(), iterations, progress=false)
	plot_out = "chain_uniform--NUTS.png"
	file_out = "output--NUTS.csv"
	hist_out_mu = "histogram--chain_uniform--NUTS--mu.png"
	hist_out_sigma = "histogram--chain_uniform--NUTS--sigma.png"
elseif solver_method=="NUTSMCMC"
	chain = sample(model, NUTS(), MCMCThreads(), iterations, 4, progress=false)
	plot_out = "chain_uniform--NUTS--MCMC4.png"
	file_out = "output--NUTS--MCMC4.csv"
	hist_out_mu = "histogram--chain_uniform--NUTS--MCMC4--mu.png"
	hist_out_sigma = "histogram--chain_uniform--NUTS--MCMC4--sigma.png"
end


mu_posterior = chain[:mumu]
sigma_posterior = chain[:sigsig]
posteriors = DataFrame(hcat(mu_posterior,sigma_posterior), :auto) #|> DataFrame

posteriors |> CSV.write(io_raw_chain; delim = ',', append=true)
close(io_raw_chain)

#describe(chain)
#plot(chain, xticks = [0:iterations, (mean_n-3*stdev_n):(mean_n+3*stdev_n), 0:iterations, (mean_n-3*stdev_n):(mean_n+3*stdev_n)])
plot(chain)
savefig(plot_out)


summary_stats_df = summarystats(chain) |> DataFrame
quantile_df = quantile(chain) |> DataFrame
#quantile_df = quantiles(chain, 0.5) |> DataFrame
#quantile_df = StatsPlots.get_quantiles(chain,[0.05, 0.1, 0.2, 0.5, 0.8, 0.9, 0.95]) |> DataFrame
#out1 = CSV.read(IOBuffer(String(summarystats(chain))), DataFrame)

#println("regular bayesian inference:")
#println(summary_stats_df)
#println(quantile_df)

summary_stats_header = Tables.table(["parameter" "mean" "stdev" "naive_se" "mcse" "ess" "rhat" "ess_per_sec"])
quantile_header = Tables.table(["parameter" "2.5%" "25%" "50%" "75%" "97.5%"])

io = open(file_out,"w")

CSV.write(io, summary_stats_header; delim = ',', header = false)
summary_stats_df |> CSV.write(io; delim = ',', append=true)
[ ] |> CSV.write(io; delim = ',', append=true)
quantile_header |> CSV.write(io; delim = ',', append=true)
quantile_df |> CSV.write(io; delim = ',', append=true)
[ ] |> CSV.write(io; delim = ',', append=true)
close(io)

text_size=18
label_size=16

mu_posterior = chain[:mumu]
sigma_posterior = chain[:sigsig]

x_prior_mu = [700, 985, 1300]
y_prior_mu = [0, 1/300., 0]

x_prior_sigma = [0, 105, 300]
y_prior_sigma = [0, 1/150., 0]


plot(xtickfontsize=text_size,
	ytickfontsize=text_size,
	xguidefontsize=text_size,
	yguidefontsize=text_size,
	legendfontsize=text_size,
	dpi=300,
	grid=false,
	framestyle = :box,
	fontfamily="Computer Modern",
	legend=false)
density!(mu_posterior, nbins=50, normalize = :probability, c = :blue)
plot!(x_prior_mu, y_prior_mu, c = :red)
plot!([1000, 1000], [0, 0.01], c = :black, linestyle = :dash, linewidth = 2)
xticks!([700, 800, 900, 1000, 1100, 1200, 1300])
yticks!([0., 0.002, 0.004, 0.006, 0.008, 0.01])
plot!(xlim = (670., 1330))
plot!(ylim = (0., 0.01))
annotate!(1005, 0.008, text("True value", text_size, :black, :left))
annotate!(800, 0.002, text("\$P(\\mu)\$", text_size, :red, :center))
annotate!(1085, 0.004, text("\$P(\\mu\\,|D)\$", text_size, :blue, :center))
plot!(ylabel="Probability (-)")
plot!(xlabel="Velocity (-)")
savefig(hist_out_mu)

plot(xtickfontsize=text_size,
	ytickfontsize=text_size,
	xguidefontsize=text_size,
	yguidefontsize=text_size,
	legendfontsize=text_size,
	dpi=300,
	grid=false,
	framestyle = :box,
	fontfamily="Computer Modern",
	legend=false)
density!(sigma_posterior, nbins=50, normalize = :probability, c = :blue)
plot!(x_prior_sigma, y_prior_sigma, c = :red)
plot!([100, 100], [0, 0.01], c = :black, linestyle = :dash, linewidth = 2)
xticks!([0, 100, 200, 300])
yticks!([0., 0.002, 0.004, 0.006, 0.008, 0.01])
plot!(xlim = (0., 310))
plot!(ylim = (0., 0.01))
annotate!(105, 0.001, text("Limit", text_size, :black, :left))
annotate!(40, 0.004, text("\$P(\\sigma)\$", text_size, :red, :center))
annotate!(182, 0.008, text("\$P(\\sigma\\,|D)\$", text_size, :blue, :center))
plot!(ylabel="Probability (-)")
plot!(xlabel="Velocity (-)")
savefig(hist_out_sigma)


